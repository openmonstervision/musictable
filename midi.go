package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type mNote []struct {
	Note string `json:"note"`
	Freq string `json:"freq"`
	Midi string `json:"midi"`
}

func main() {
	jsonFile, err := os.Open("mNotes.json")
	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var mNotes mNote
	json.Unmarshal(byteValue, &mNotes)
	for i := 0; i < len(mNotes); i++ {
		if string(os.Args[1]) == string(mNotes[i].Midi) {
			fmt.Println(mNotes[i].Freq)
			fmt.Println(mNotes[i].Note)
		}
	}
}
