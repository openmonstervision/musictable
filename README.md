# README #

Convert notes into frequency and midi number

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

git clone repo
```
go build note.go
go build midi.go
```
Then to use just run the program and a note:
```
dusty@pop-os:~/git/musictable$ ./note A#1
58.270
34


dusty@pop-os:~/git/musictable$ ./midi 64
329.628
E4

```
Recieve frequency and midinote